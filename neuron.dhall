{ siteTitle = "Scribbles"
, author = Some "Victor Albertsson"
, siteBaseUrl = Some "https://victoralbertsson.gitlab.io/scribbles"
-- List of themes: https://semantic-ui.com/usage/theming.html#sitewide-defaults
, theme = "black"
, editUrl = Some "https://gitlab.com/victoralbertsson/scribbles/edit/master/"
, mathJaxSupport = False
}
