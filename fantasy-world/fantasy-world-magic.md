# Magic

## Foundational Schools of Sorcery

Firebreather
: Color: Red
: Elements: Fire, Earth

Mistwalker
: Color: Green
: Elements: Earth, Air

Stormcaller
: Color: Blue
: Elements: Air, Fire

Lightspinner
: Color: White
: Elements: Unity, Matter

Dreamweaver
: Color: Black
: Elements: Adhesion, Mind

## Classification of Divine Miracles

### Common Miracles of the Pantheon

[Status]
: Displays a summary of the targets condition to the user.
: Can also be used on oneself.

[Identify]
: Reveals the targets name and affiliation to the user.

[Conceal Identity]
: Spoofs the users [Identify] results.

[Conceal Essence]
: Spoofs the users [Status] results.
: Advanced version of [Conceal Identity].

[Evolve]
: Used by [[fantasy-rpg-heteromorph|heteromorphs]] to show the evolution interface.
: Can only be used when the race-specific "mutation" requirements are met.

### God of War and Honor

[Glory Eternal]
: Restores the target to pre-combat condition including any items on their person.

### God of Chaos and Commerce

### God of Fertility and Harmony

[Unity of Mind]
: Enables the user to telepathically communicate with the target.

### God of Truth and Order

[Reveal]
: Reveals the targets [Status], bypassing all illusion.
: If used on an object it imitates [Appraise] instead.
