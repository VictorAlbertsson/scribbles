# Politics

## Eura Continent

Horizontal contiguous landmass, located south of the Karn continent separated by a narrow sea.

### Polotep Empire (Egyptian)

#### Provinces

Achaea
: Capital

### Harzagrib Empire (Mesopotamian/Assyrian)

#### Provinces

Kapp
: Capital

### Hekip Kingdom (Ethiopian)

#### Provinces

Ylovna
: Capital

## Karn Continent

Horizontal mostly contiguous landmass, located north of the Eura continent separated by a narrow sea.
Although Karn is very similar in shape to Eura the northern coasts are very fragmented resulting in various peninsulas, islands and archipelagos.

### Marna Empire (Roman)

#### Provinces

Krylos
: Capital
: Former Province of Ekypos

Epna
: Former Capital

### Ekypos Republic (Greek)

#### Provinces

Heptna
: Capital
