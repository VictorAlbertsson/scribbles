---
tags: [language]
---

# Semantics

1. Lazy evaluation (might clash with 3; subject to change)
2. Automatic vectorization
3. Conservative memory management
4. "Values have types not variables" (compile-time attributes vs. static types)
