---
tags: [language]
---

# Syntax

## Level 1

	Tuple     := ( <Expression> <<,>> )
	Array     := [ <Expression> <<,>> ]
	Structure := { <Assignment> <<,>> }
	Lambda    := <Tuple> <Structure>

## Level 2

	Assignment := <Symbol> : <Expression>
	Pipe       := <Expression> | <Expression>
